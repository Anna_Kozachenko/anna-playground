import org.mockito.MockingDetails;
import org.mockito.Mockito;
import org.mockito.invocation.Invocation;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.fail;

/**
 * This class is meant to be used by the unit tests. Its goal is to detect whether all setters (inherited or not) of
 * a particular class were invoked. If at lest one setter is not invoked, an error is reported back.
 */
public class ClassMethodVerifier {

    private static final String METHOD_NOT_INVOKED = "%s method(s) not invoked: %s";

    /**
     * Verifies that all {@link Method}s in {@link Class} classToInspect (even the inherited) are invoked.
     *
     * If at least 1 {@link Method} is not called, the function fails providing the list of all s{@link Method}s
     * that were not invoked.
     *
     * @see Class#getMethods()
     *
     * @param classToInspect class of interest
     * @param objectToInspect object of interest
     * @param prefix prefix to filter upon
     */
    public static void verifyAllMethodsAreInvoked(Class classToInspect, Object objectToInspect, String prefix) {
        List<String> aMethods = getAllMethodsForPrefix(classToInspect, prefix);
        List<String> iMethods = getInvokedMethods(objectToInspect, prefix);

        List<String> niMethods = aMethods.stream().filter(m -> !iMethods.contains(m)).collect(Collectors.toList());
        if (!niMethods.isEmpty()) {
            fail(String.format(METHOD_NOT_INVOKED, niMethods.size(), niMethods.toString()));
        }
    }

    /**
     * Verifies that all declared {@link Method}s in {@link Class} classToInspect are invoked. Note that inherited
     * methods are ignored. It is typically used to test enriched or virtual fields mappers.
     *
     * If at least 1 {@link Method} is not called, the function fails providing the list of all declared {@link Method}s
     * that were not invoked.
     *
     * @see Class#getDeclaredMethods()
     *
     * @param classToInspect class of interest
     * @param objectToInspect object of interest
     * @param prefix prefix to filter upon
     */
    public static void verifyAllDeclaredMethodsAreInvoked(Class classToInspect, Object objectToInspect, String prefix) {
        List<String> dMethods = getDeclaredMethodsForPrefix(classToInspect, prefix);
        List<String> iMethods = getInvokedMethods(objectToInspect, prefix);

        List<String> niMethods = dMethods.stream().filter(m -> !iMethods.contains(m)).collect(Collectors.toList());
        if (!niMethods.isEmpty()) {
            fail(String.format(METHOD_NOT_INVOKED, niMethods.size(), niMethods.toString()));
        }
    }

    /**
     * Selects only the declared {@link Method}s of a particular {@link Class} with a particular prefix.
     * Note that inherited methods are ignored.
     *
     * @param c class of interest
     * @param prefix prefix to filter upon
     * @return
     */
    private static List<String> getDeclaredMethodsForPrefix(Class c, String prefix) {
        if (c == null) {
            return new ArrayList<>();
        }

        return getMethodsForPrefix(c.getDeclaredMethods(), prefix);
    }

    /**
     * Selects all the {@link Method}s (even the inherited) of a particular {@link Class} with a particular prefix.
     *
     * @param c class of interest
     * @param prefix prefix to filter upon
     * @return
     */
    private static List<String> getAllMethodsForPrefix(Class c, String prefix) {
        if (c == null) {
            return new ArrayList<>();
        }

        return getMethodsForPrefix(c.getMethods(), prefix);
    }

    /**
     * Selects the {@link Method}s that were actually invoked on a particular {@link Object} and that have a particular
     * prefix.
     *
     * @param toInspect object of interest
     * @param prefix prefix to filter upon
     * @return
     */
    private static List<String> getInvokedMethods(Object toInspect, String prefix) {
        if (toInspect == null) {
            return new ArrayList<>();
        }

        MockingDetails mockingDetails = Mockito.mockingDetails(toInspect);
        return getInvocationsForPrefix(mockingDetails.getInvocations(), prefix);
    }

    /**
     * Selects {@link Method} names with a particular prefix.
     *
     * @param methods an array of methods
     * @param prefix a prefix to filter upon
     * @return Filtered list of {@link String}s that represents method names
     */
    private static List<String> getMethodsForPrefix(Method[] methods, String prefix) {
        return Arrays.stream(methods)
                // Take into account only method names
                .map(Method::getName)
                // ...that contain 'prefix'
                .filter(m -> prefix == null || m.startsWith(prefix))
                //...and are sorted in the natural order
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * Selects {@link Invocation} names with a particular prefix.
     *
     * @param invocations a collection of invocations
     * @param prefix a prefix to filter upon
     * @return Filtered list of {@link String}s that represents invoked method names
     */
    private static List<String> getInvocationsForPrefix(Collection<Invocation> invocations, String prefix) {
        return invocations.stream()
                // Take into account only method names
                .map(i -> i.getMethod().getName())
                // ...that contain 'prefix'
                .filter(m -> prefix == null || m.startsWith(prefix))
                // ...and are sorted in the natural order
                .sorted()
                .collect(Collectors.toList());
    }
}