import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class ClassMethodVerifierTest {

    private static final String PREFIX = "set";

    MyClass myObject;

    @Before
    public void setup() {
        myObject = Mockito.mock(MyClass.class);
    }

    @Test
    public void verifyClassToInspectNullDeclared() {
        ClassMethodVerifier.verifyAllDeclaredMethodsAreInvoked(null, myObject, PREFIX);
    }

    @Test
    public void verifyClassToInspectNullAllMethods() {
        ClassMethodVerifier.verifyAllMethodsAreInvoked(null, myObject, PREFIX);
    }

    @Test
    public void verifyObjectToInspectNullDeclared() {
        try {
            ClassMethodVerifier.verifyAllDeclaredMethodsAreInvoked(MyClass.class, null, PREFIX);
        } catch (AssertionError e) {
            Assert.assertEquals("2 method(s) not invoked: [setFieldA, setFieldB]", e.getMessage());
        }
    }

    @Test
    public void verifyObjectToInspectNullAllMethods() {
        try {
            ClassMethodVerifier.verifyAllMethodsAreInvoked(MyClass.class, null, PREFIX);
        } catch (AssertionError e) {
            Assert.assertEquals("4 method(s) not invoked: [setFieldA, setFieldB, setParentFieldA, " +
                    "setParentFieldB]", e.getMessage());
        }
    }

    @Test
    public void verifyPrefixNullDeclared() {
        try {
            ClassMethodVerifier.verifyAllDeclaredMethodsAreInvoked(MyClass.class, myObject, null);
        } catch (AssertionError e) {
            Assert.assertEquals("4 method(s) not invoked: [getFieldA, getFieldB, setFieldA, setFieldB]",
                    e.getMessage());
        }
    }

    @Test
    public void verifyPrefixNullAllMethods() {
        try {
            ClassMethodVerifier.verifyAllMethodsAreInvoked(MyClass.class, myObject, null);
        } catch (AssertionError e) {
            Assert.assertEquals("15 method(s) not invoked: [equals, getClass, getFieldA, getFieldB, " +
                            "hashCode, notify, notifyAll, setFieldA, setFieldB, setParentFieldA, setParentFieldB, " +
                            "toString, wait, wait, wait]",
                    e.getMessage());
        }
    }

    @Test
    public void verifyDeclaredOneMethodInvoked() {
        myObject.setFieldA(Byte.MIN_VALUE);

        try {
            ClassMethodVerifier.verifyAllDeclaredMethodsAreInvoked(MyClass.class, myObject, PREFIX);
        } catch (AssertionError e) {
            Assert.assertEquals("1 method(s) not invoked: [setFieldB]", e.getMessage());
        }
    }

    @Test
    public void verifyDeclaredAllMethodInvoked() {
        myObject.setFieldA(Byte.MIN_VALUE);
        myObject.setFieldB(Byte.MIN_VALUE);

        ClassMethodVerifier.verifyAllDeclaredMethodsAreInvoked(MyClass.class, myObject, PREFIX);
    }

    @Test
    public void verifyMethodsNoInheritedMethodInvoked() {
        // Set all declared methods but no inherited methods
        myObject.setFieldA(Byte.MIN_VALUE);
        myObject.setFieldB(Byte.MIN_VALUE);

        try {
            ClassMethodVerifier.verifyAllMethodsAreInvoked(MyClass.class, myObject, PREFIX);
        } catch (AssertionError e) {
            Assert.assertEquals("2 method(s) not invoked: [setParentFieldA, setParentFieldB]", e.getMessage());
        }
    }

    @Test
    public void verifyMethodsOneInheritedMethodInvoked() {
        // Set all declared methods
        myObject.setFieldA(Byte.MIN_VALUE);
        myObject.setFieldB(Byte.MIN_VALUE);
        // Set 1 out of 2 inherited methods
        myObject.setParentFieldA(Byte.MIN_VALUE);

        try {
            ClassMethodVerifier.verifyAllMethodsAreInvoked(MyClass.class, myObject, PREFIX);
        } catch (AssertionError e) {
            Assert.assertEquals("1 method(s) not invoked: [setParentFieldB]", e.getMessage());
        }
    }

    @Test
    public void verifyMethodsAllInheritedMethodInvoked() {
        // Set all declared methods
        myObject.setFieldA(Byte.MIN_VALUE);
        myObject.setFieldB(Byte.MIN_VALUE);
        // Set all inherited methods
        myObject.setParentFieldA(Byte.MIN_VALUE);
        myObject.setParentFieldB(Byte.MIN_VALUE);

        ClassMethodVerifier.verifyAllMethodsAreInvoked(MyClass.class, myObject, PREFIX);
    }

    class MyClass extends MyParentClass {

        private byte fieldA;
        private byte fieldB;

        public MyClass(byte fieldA, byte fieldB, byte parentFieldA, byte parentFieldB) {
            super(parentFieldA, parentFieldB);
            this.fieldA = fieldA;
            this.fieldB = fieldB;
        }

        public byte getFieldA() {
            return fieldA;
        }

        public void setFieldA(byte fieldA) {
            this.fieldA = fieldA;
        }

        public byte getFieldB() {
            return fieldB;
        }

        public void setFieldB(byte fieldB) {
            this.fieldB = fieldB;
        }
    }

    class MyParentClass {

        private byte parentFieldA;
        private byte parentFieldB;

        public MyParentClass(byte parentFieldA, byte parentFieldB) {
            this.parentFieldA = parentFieldA;
            this.parentFieldB = parentFieldB;
        }

        public void setParentFieldA(byte parentFieldA) {
            this.parentFieldA = parentFieldA;
        }

        public void setParentFieldB(byte parentFieldB) {
            this.parentFieldB = parentFieldB;
        }
    }
}
