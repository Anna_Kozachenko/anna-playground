package jsonbuilder;

/**
 * API they can be used to interact with a search engine.
 *
 * API uses the Builder pattern to create the below JSON structure. This JSON structure is actually a query that can be
 * submitted to a search engine, i.e. Elasticsearch.
 *
 * JSON structure example:
 * {
 *      "query": {
 *          "bool": {
 *              "must": [
 *                  { "match": { "item": "Milk" }},
 *                  { "match": { "item_type": "Dairy" }}
 *              ],
 *              "should": [
 *                  { "match": { "product_location": "New Mexico" }},
 *                  { "match": { "warehouse_number": 37 }}
 *              ]
 *          }
 *      }
 * }
 *
 * Here are the rules for the JSON structure. You can have a single 'must' or 'should' section inside a 'bool' section
 * as shown. But keep in mind that inside of each one of these 'must' or 'should' sections, you can have nested 'bool'
 * sections. The 'match' section is simple, you can have any attribute name and it's value. For example, the above json
 * query is filtering for ONLY those items that are "Milk". And the "item_type" attribute has the value "Dairy".
 * The product_location should be "New Mexico" with warehouse_number: 37.
 *
 * To convert your classes into json you'll have to use an external java library called Jackson.
 * You can learn more about it here: http://www.mkyong.com/java/how-to-convert-java-object-to-from-json-jackson/
 *
 * Online JSON editor: http://www.jsoneditoronline.org/
 */
public class Application {

    public static void main(String[] args) {
        QueryBuilder builder = new QueryBuilder();
       // builder.bool().mustMatch("item", "Milk").mustMatch("item_type", "Dairy");
       // builder.bool().shouldMatch("product_location", "New Mexico").shouldMatch("warehouse_number", 37);

        // example of the nested 'bool' block
       // builder.bool().shouldMatch("lot_number", 307).bool().mustMatch("expiry_date", "Jan 2020");
    }
}
