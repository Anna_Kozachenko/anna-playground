import com.google.common.base.Strings;

/**
 * Class responsible for evaluating whether the input {@link String} is a palindrome or not where palindrome is a word,
 * phrase, or sequence that reads the same backwards as forwards, e.g. 'madam' or 'nurses run'.
 */
public class PalindromeChecker {

    public static void main(String[] args) {
        isPalindrome("madam");
        isPalindrome("nurses run");
    }

    /**
     * Evaluates whether the input {@link String} is a palindrome by splitting the input into chars and calculating
     * a total hash value. While iterating through the left half of the input, the total hash value is summed with every
     * new char. However, while iterating through the right half, every new char is subtracted from the total hash
     * value. Therefore, if the input is a palindrome, the total hash value after the full iteration should be 0.
     *
     * @param input The input {@link String} to be evaluated
     * @return true if the input {@link String} is a palindrome, false otherwise
     */
    public static boolean isPalindrome(String input) {
        if (Strings.isNullOrEmpty(input)) {
            return false;
        }

        // 'Integer' needs to be used here instead of 'int' in order to handle the long input Strings (signed int goes
        // only up to 2^31 - 1 =  2147483647 decimal
        Integer totalHash = 0;
        Character[] preparedInput = prepareInput(input);
        int half = preparedInput.length / 2, modulo = preparedInput.length % 2;

        for (int i = 0; i < preparedInput.length; i++) {

            // If input contains an odd number of letters, then the letter in the middle should be skipped
            if (i == half && modulo != 0) {
                continue;
            }

            // Process left part of the input: increase hash value
            if (i < half) {
                totalHash += preparedInput[i] * (int)Math.pow(13, half - i - 1);
            // Process right part of the input: decrease hash value
            } else {
                totalHash -= preparedInput[i] * (int)Math.pow(13, i - half - modulo);
            }
        }

        System.out.println(String.format("Is '%s' a palindrome? %s.", input, totalHash == 0 ? "Yes" : "No"));

        return totalHash == 0;
    }

    /**
     * Removes all the characters from the input {@link String} that do not need to be taken into account when
     * evaluating the total hash value, i.e. space, dot, comma, etc.
     *
     * @param input The input {@link String} to be cleaned-up
     * @return array of {@link Character}s that are ready to be processed
     */
    private static Character[] prepareInput(String input) {
        return input
                .chars()
                .mapToObj(c -> (char)c)
                // Remove the spaces ' '
                .filter(c -> c != ' ')
                // Remove the dots '.'
                .filter(c -> c != '.')
                // Remove the commas ','
                .filter(c -> c != ',')
                // Remove the colons ':'
                .filter(c -> c != ':')
                .toArray(Character[]::new);
    }
}