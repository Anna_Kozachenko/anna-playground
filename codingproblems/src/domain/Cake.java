package domain;

/**
 * Cake domain object.
 */
public class Cake implements Comparable<Cake> {
    private int weight;
    private int value;
    private int valueForOneKilo;

    public Cake(int weight, int value) {
        this.weight = weight;
        this.value = value;
        // Be careful with division by zero !!!
        this.valueForOneKilo = weight > 0 ? value / weight : 0;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getValueForOneKilo() {
        return valueForOneKilo;
    }

    public void setValueForOneKilo(int valueForOneKilo) {
        this.valueForOneKilo = valueForOneKilo;
    }

    // To sort in descending order (from biggest to smallest).
    public int compareTo(Cake other) {
        return this.valueForOneKilo > other.valueForOneKilo ? -1 :
                this.valueForOneKilo < other.valueForOneKilo ? 1 : 0;
    }

    @Override
    public String toString() {
        return "{w=" + weight + ", v=" + value + ", vF1Kg=" + valueForOneKilo + "}";
    }
}