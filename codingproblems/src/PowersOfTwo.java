import java.util.ArrayList;
import java.util.List;

/**
 * The following class is responsible for finding the powers of 2 from 1 through n (inclusive).
 * For example, if n = 4, it prints 1, 2 and 4.
 *
 * The runtime is the number of times we can divide n by 2 until we get down to 1.
 * The number of times we can half n until we get 1 is O(logn).
 */
public class PowersOfTwo {

    public static void main (String[] args) {
        List<Integer> result = new ArrayList<>();
        powersOfTwo(50, result);
        System.out.println(result);
    }

    public static int powersOfTwo(int n, List<Integer> result) {
        if (n <= 0) {
            result.add(0);
            return 0;
        } else if (n == 1) {
            result.add(1);
            return 1;
        } else {
            int previous = powersOfTwo(n / 2, result);
            int current = previous * 2;

            result.add(current);
            return current;
        }
    }
}
