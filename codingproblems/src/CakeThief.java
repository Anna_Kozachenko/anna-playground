package domain;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
* You are a renowned thief who has recently switched from stealing precious metals to stealing cakes because of
* the insane profit margins. You end up hitting the jackpot, breaking into the world's largest privately owned stock of
* cakes—the vault of the Queen of England.
*
* While Queen Elizabeth has a limited number of types of cake, she has an unlimited supply of each type.
*
* Each type of cake has a weight and a value, stored in objects of a Cake class.
* For example:
* new CakeType(7, 160); // Weighs 7 kilograms and has a value of 160 pounds
*
* You brought a duffel bag that can hold limited weight, and you want to make off with the most valuable haul possible.
*
* Write a function maxDuffelBagValue() that takes a list of cakes and a weight capacity, and returns the maximum
* monetary value the duffel bag can hold.
* For example:
* List<Cake> cakeTuples =
* int capacity =
* int max = maxDuffelBagValue(cakeTuples, capacity); // Returns
*
* Weights and values may be any non-negative integer.
*
* GOTCHAS:
* We can do this in O(n*k)O(n∗k) time and O(k)O(k) space, where n is the number of types of cakes and k is the duffel
* bag's capacity!
* */
public class CakeThief {

    public static void main(String[] args) {
        List<Cake> cakeTuples = Arrays.asList(new Cake(1, 30), new Cake(50, 200));
        int capacity = 100;
        CakeThief.maxDuffelBagValue(cakeTuples, capacity);
    }

    /**
    * How can we ensure we get the optimal value we can carry?
    * Try thinking small. How can we calculate the maximum value for a duffel bag with a weight capacity of 1 kg?
    *
    * If the capacity is 1 kg, we’ll only care about cakes that weigh 1 kg (for simplicity, let's ignore zeroes for
     * now). And we'd just want the one with the highest value.
    *
    * What if the capacity is 2 kg? We care about cakes that weigh 1 or 2 kg.
    * - If the cake weighs 2 kg, it would fill up our whole capacity if we just took one.So we just need to see if
     * the cake's value is higher than our current maxValueAtCapacity.
    * - If the cake weighs 1 kg, we could take one, and we'd still have 1 kg of capacity left.
    * How do we know the best way to fill that extra capacity? We can use the max value at capacity
    * We’ll see if adding the cake's value to the max value at capacity 1 is better than our current maxValueAtCapacity.
    */
    public static long maxDuffelBagValue(List<Cake> cakes, int capacity) {
        // We create an array to hold the maximum possible value at every duffel bag weight capacity from 0 to
        // weightCapacity starting each index with value 0
        int[] maxValuesAtCapacities = new int[capacity + 1];

        for (int currentCapacity = 0; currentCapacity <= capacity; currentCapacity++) {

            // Set a variable to hold the max monetary value so far for currentCapacity
            int currentMaxValue = 0;

            for (Cake cake : cakes) {

                // If the current cake weighs as much or less than the current weight capacity, it's possible taking
                // the cake would give get a better value
                if (cake.getWeight() <= currentCapacity) {

                    // We check: should we use the cake or not?
                    // if we use the cake, the most kilograms we can include in addition to the cake we're adding is
                    // the current capacity minus the cake's weight. we find the max value at that integer capacity in
                    // our array maxValuesAtCapacities
                    int maxValueUsingCake = cake.getValue() + maxValuesAtCapacities[currentCapacity - cake.getWeight()];

                    // Now we see if it's worth taking the cake. how does the value with the cake compare to
                    // the currentMaxValue?
                    currentMaxValue = Math.max(maxValueUsingCake, currentMaxValue);
                }
            }

            // Add each capacity's max value to our array so we can use them when calculating all the remaining
            // capacities
            maxValuesAtCapacities[currentCapacity] = currentMaxValue;

            System.out.print(currentCapacity + ":" + maxValuesAtCapacities[currentCapacity] + " ");
        }
        System.out.println();

        return maxValuesAtCapacities[capacity];
    }

    /**
     * Brute force version of the algorithm.
     *
     * Complexity:
     * - c = # of cakes.
     *
     * O(c*logc) + O(c)
     */
    public static int maxDuffelBagValueBrute(List<Cake> cakes, int capacity) {
        if (cakes == null || cakes.isEmpty() || capacity <= 0) {
            return 0;
        }

        Collections.sort(cakes);
        System.out.println(cakes);

        int maxValue = 0;
        int capacityLeft = capacity;
        for(Cake cake: cakes) {
            if (cake.getWeight() <= 0 || cake.getValue() <= 0) {
                continue;
            }

            int maxOfCakesICanTake = capacityLeft / cake.getWeight();
            if (maxOfCakesICanTake == 0) {
                continue;
            }

            capacityLeft -= cake.getWeight() * maxOfCakesICanTake;
            maxValue += maxOfCakesICanTake * cake.getValue();
            System.out.print("I can take " + maxOfCakesICanTake + " cake(s) " + cake.getValue() + " each. Total is " +
                    maxOfCakesICanTake * cake.getValue() + ". ");
            System.out.println("Place(s) left " + capacityLeft);
        }

        return maxValue;
    }
}