import domain.Cake;
import domain.CakeThief;
import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CakeThiefTest {

    List<Cake> cakeTuples;
    int capacity;

    // Test maxDuffelBagValueBrute variant of the solution

    @Test
    public void maxDuffelBagValueBruteCapacityZero() {
        cakeTuples = Arrays.asList(new Cake(1, 160));
        capacity = 0;
        int maxDuffelBagValue = CakeThief.maxDuffelBagValueBrute(cakeTuples, capacity);
        Assert.assertEquals(0, maxDuffelBagValue);
    }

    @Test
    public void maxDuffelBagValueBruteCakesNull() {
        cakeTuples = null;
        capacity = 1;
        int maxDuffelBagValue = CakeThief.maxDuffelBagValueBrute(cakeTuples, capacity);
        Assert.assertEquals(0, maxDuffelBagValue);
    }

    @Test
    public void maxDuffelBagValueBruteCakesEmpty() {
        cakeTuples = new ArrayList<>();
        capacity = 1;
        int maxDuffelBagValue = CakeThief.maxDuffelBagValueBrute(cakeTuples, capacity);
        Assert.assertEquals(0, maxDuffelBagValue);
    }

    @Test
    public void maxDuffelBagValueBruteCakeWeightZero() {
        cakeTuples = Arrays.asList(new Cake(0, 160), new Cake(0, 90), new Cake(0, 15));
        capacity = 20;
        int maxDuffelBagValue = CakeThief.maxDuffelBagValueBrute(cakeTuples, capacity);
        Assert.assertEquals(0, maxDuffelBagValue);
    }

    @Test
    public void maxDuffelBagValueBruteCakeValueZero() {
        cakeTuples = Arrays.asList(new Cake(7, 0), new Cake(3, 0), new Cake(2, 0));
        capacity = 20;
        int maxDuffelBagValue = CakeThief.maxDuffelBagValueBrute(cakeTuples, capacity);
        Assert.assertEquals(0, maxDuffelBagValue);
    }

    @Test
    public void maxDuffelBagValueBrute() {
        cakeTuples = Arrays.asList(new Cake(7, 160), new Cake(3, 90), new Cake(2, 15));
        capacity = 20;
        int maxDuffelBagValue = CakeThief.maxDuffelBagValueBrute(cakeTuples, capacity);
        Assert.assertEquals(555, maxDuffelBagValue);
    }

    /**
     * Example of how it is important to take the RATIO value/weight and not the biggest VALUE!
     */
    @Test
    public void maxDuffelBagValueBrute1() {
        cakeTuples = Arrays.asList(new Cake(1, 30), new Cake(50, 200));
        capacity = 100;
        int maxDuffelBagValue = CakeThief.maxDuffelBagValueBrute(cakeTuples, capacity);
        Assert.assertEquals(3000, maxDuffelBagValue);
    }

    /**
     * We might run into problems if the weight of the cake with the highest value/weight ratio does not fit evenly
     * into the capacity.
     */
    @Test
    public void maxDuffelBagValueBrute2() {
        cakeTuples = Arrays.asList(new Cake(3, 40), new Cake(5, 70));

        int maxDuffelBagValue = 110;

        int maxDuffelBagValue1 = CakeThief.maxDuffelBagValueBrute(cakeTuples, 8);
        Assert.assertEquals(maxDuffelBagValue, maxDuffelBagValue1);

        int maxDuffelBagValue2 = CakeThief.maxDuffelBagValueBrute(cakeTuples, 9);
        Assert.assertEquals(maxDuffelBagValue, maxDuffelBagValue2);
    }


    // Test maxDuffelBagValue variant of the solution that covers all the cases

    @Test
    public void maxDuffelBagValue() {
        cakeTuples = Arrays.asList(new Cake(7, 160), new Cake(3, 90), new Cake(2, 15));
        capacity = 20;
        long maxDuffelBagValue = CakeThief.maxDuffelBagValue(cakeTuples, capacity);
        Assert.assertEquals(555, maxDuffelBagValue);
    }

    /**
     * Example of how it is important to take the RATIO value/weight and not the biggest VALUE!
     */
    @Test
    public void maxDuffelBagValue1() {
        cakeTuples = Arrays.asList(new Cake(1, 30), new Cake(50, 200));
        capacity = 100;
        long maxDuffelBagValue = CakeThief.maxDuffelBagValue(cakeTuples, capacity);
        Assert.assertEquals(3000, maxDuffelBagValue);
    }

    /**
     * This variant of the solution solves the problem that the weight of the cake with the highest value/weight ratio
     * does not fit evenly into the capacity.
     */
    @Test
    public void maxDuffelBagValue2() {
        cakeTuples = Arrays.asList(new Cake(3, 40), new Cake(5, 70));

        long maxDuffelBagValue1 = CakeThief.maxDuffelBagValue(cakeTuples, 8);
        Assert.assertEquals(110, maxDuffelBagValue1);

        long maxDuffelBagValue2 = CakeThief.maxDuffelBagValue(cakeTuples, 9);
        Assert.assertEquals(120, maxDuffelBagValue2);
    }
}
