import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PowersOfTwoTest {

    List<Integer> result;

    @Before
    public void setup() {
        result = new ArrayList<>();
    }

    @Test
    public void powersOfTwoNNegative() {
        PowersOfTwo.powersOfTwo(-10, result);
        Assert.assertEquals(Arrays.asList(0), result);
    }

    @Test
    public void powersOfTwoNZero() {
        PowersOfTwo.powersOfTwo(0, result);
        Assert.assertEquals(Arrays.asList(0), result);
    }

    @Test
    public void powersOfTwoNOne() {
        PowersOfTwo.powersOfTwo(1, result);
        Assert.assertEquals(Arrays.asList(1), result);
    }

    @Test
    public void powersOfTwoNFour() {
        PowersOfTwo.powersOfTwo(4, result);
        Assert.assertEquals(Arrays.asList(1, 2, 4), result);
    }

    @Test
    public void powersOfTwoNFifty() {
        PowersOfTwo.powersOfTwo(50, result);
        Assert.assertEquals(Arrays.asList(1, 2, 4, 8, 16, 32), result);
    }
}
