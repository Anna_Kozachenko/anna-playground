import org.junit.Assert;
import org.junit.Test;

public class PalindromeCheckerTest {

    @Test
    public void nullInput() {
        boolean isPalindrome = PalindromeChecker.isPalindrome(null);
        Assert.assertFalse(isPalindrome);
    }

    @Test
    public void emptyInput() {
        boolean isPalindrome = PalindromeChecker.isPalindrome("");
        Assert.assertFalse(isPalindrome);
    }

    @Test
    public void oneWordPalindrome() {
        boolean isPalindrome = PalindromeChecker.isPalindrome("anna");
        Assert.assertTrue(isPalindrome);
    }

    @Test
    public void oneWordNotPalindrome() {
        boolean isPalindrome = PalindromeChecker.isPalindrome("time");
        Assert.assertFalse(isPalindrome);
    }

    @Test
    public void oneOddWordPalindrome() {
        boolean isPalindrome = PalindromeChecker.isPalindrome("madam");
        Assert.assertTrue(isPalindrome);
    }

    @Test
    public void oneOddWordNotPalindrome() {
        boolean isPalindrome = PalindromeChecker.isPalindrome("program");
        Assert.assertFalse(isPalindrome);
    }

    @Test
    public void palindromeWithSpace() {
        boolean isPalindrome = PalindromeChecker.isPalindrome("nurses run");
        Assert.assertTrue(isPalindrome);
    }

    @Test
    public void palindromeWithSpaceDot() {
        boolean isPalindrome = PalindromeChecker.isPalindrome("a but tuba.");
        Assert.assertTrue(isPalindrome);
    }

    @Test
    public void palindromeWithSpaceDotComma() {
        boolean isPalindrome = PalindromeChecker.isPalindrome("a car, a man, a maraca.");
        Assert.assertTrue(isPalindrome);
    }

    @Test
    public void palindromeWithSpaceDotCommaColon() {
        boolean isPalindrome = PalindromeChecker.isPalindrome("a dog, a plan, a canal: pagoda.");
        Assert.assertTrue(isPalindrome);
    }
}
