package designpattern.template;

import designpattern.template.domain.CashTransaction;
import designpattern.template.domain.Forecast;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class ValidatorTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }

    @Test
    public void validateCashTransaction() {
        Validator validator = new CashTransactionValidator();
        validator.validate(new CashTransaction());

        assertEquals(
                "Validation #1 for CashTransaction\nValidation #2 for CashTransaction\n",
                outContent.toString());
    }

    @Test
    public void validateForecast() {
        Validator validator = new ForecastValidator();
        validator.validate(new Forecast());

        assertEquals(
                "Validation #1 for Forecast\nValidation #2 for Forecast\n",
                outContent.toString());
    }
}
