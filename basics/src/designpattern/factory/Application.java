package designpattern.factory;

import designpattern.factory.domain.Vehicle;
import designpattern.factory.factories.VehicleFactory;

import static designpattern.factory.enums.VehicleType.BUS;

public class Application {

    public static void main(String[] args) {
        VehicleFactory factory = new VehicleFactory();

        Vehicle bus = factory.getVehicle(BUS);
        bus.startEngine();
    }
}
