package designpattern.factory.enums;

import designpattern.factory.domain.Bus;
import designpattern.factory.domain.Car;
import designpattern.factory.domain.Truck;
import designpattern.factory.domain.Vehicle;

public enum VehicleType {

    BUS {
        public Vehicle getVehicle() {
            return new Bus();
        }
    },
    TRUCK {
        public Vehicle getVehicle() {
            return new Truck();
        }
    },
    CAR {
        public Vehicle getVehicle() {
            return new Car();
        }
    };

    public abstract Vehicle getVehicle();
}
