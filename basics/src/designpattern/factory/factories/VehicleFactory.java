package designpattern.factory.factories;

import designpattern.factory.domain.Vehicle;
import designpattern.factory.enums.VehicleType;

public class VehicleFactory {

    public Vehicle getVehicle(VehicleType vt) {
        return vt.getVehicle();
    }
}
