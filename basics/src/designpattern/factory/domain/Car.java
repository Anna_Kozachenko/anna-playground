package designpattern.factory.domain;

public class Car implements Vehicle {

    @Override
    public void startEngine() {
        System.out.println("Car: engine is started.");
    }
}
