package designpattern.factory.domain;

public interface Vehicle {

    void startEngine();
}
