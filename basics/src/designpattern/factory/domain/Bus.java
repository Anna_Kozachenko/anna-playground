package designpattern.factory.domain;

public class Bus implements Vehicle {

    @Override
    public void startEngine() {
        System.out.println("Bus: engine is started.");
    }
}
