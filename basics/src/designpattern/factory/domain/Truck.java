package designpattern.factory.domain;

public class Truck implements Vehicle {

    @Override
    public void startEngine() {
        System.out.println("Truck: engine is started.");
    }
}
