package designpattern.builder.builders;

public class User {

    private String userName; // required
    private String email; // required

    private String firstName; // optional
    private String lastName; // optional
    private int phoneNumber; // optional
    private String address; // optional

    private User(Builder b) {
        this.userName = b.userName;
        this.email = b.email;
        this.firstName = b.firstName;
        this.lastName = b.lastName;
        this.phoneNumber = b.phoneNumber;
        this.address = b.address;
    }

    public static class Builder {

        private String userName; // required
        private String email; // required

        private String firstName; // optional
        private String lastName; // optional
        private int phoneNumber; // optional
        private String address; // optional

        public Builder(String userName, String email) {
            this.userName = userName;
            this.email = email;
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder phoneNumber(int phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", address='" + address + '\'' +
                '}';
    }
}
