package designpattern.builder;

import designpattern.builder.builders.User;

public class Application {

    public static void main(String[] args) {
        User bob = new User.Builder("bobMax", "bobMax@gmail.com")
                .firstName("Bob")
                .lastName("Smith")
                .build();
        System.out.println(bob);
    }
}
