package designpattern.singleton;

public class PerformanceStage {

    private static PerformanceStage INSTANCE;

    // Private constructor for Singleton
    private PerformanceStage() {}

    public static PerformanceStage getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PerformanceStage();
        }

        return INSTANCE;
    }
}
