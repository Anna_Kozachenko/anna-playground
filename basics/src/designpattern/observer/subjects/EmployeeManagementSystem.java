package designpattern.observer.subjects;

import designpattern.observer.domain.Employee;
import designpattern.observer.observers.IObserver;

import java.util.ArrayList;
import java.util.List;

public class EmployeeManagementSystem implements ISubject {

    List<Employee> employees;
    List<IObserver> observers;

    public EmployeeManagementSystem() {
        this.employees = new ArrayList<>();
        this.observers = new ArrayList<>();
    }

    public void hireEmployee(Employee e) {
        System.out.println("New hire: " + e);
        employees.add(e);
        notifyObservers();
    }

    @Override
    public void registerObserver(IObserver o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(IObserver o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(IObserver::notifyObserver);
    }
}
