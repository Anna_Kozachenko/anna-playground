package designpattern.observer.observers;

public class SalesDepartment implements IObserver {

    @Override
    public void notifyObserver() {
        System.out.println("Sales department is notified");
    }
}
