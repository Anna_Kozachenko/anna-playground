package designpattern.observer.observers;

public interface IObserver {

    void notifyObserver();
}
