package designpattern.observer.observers;

public class AccountingDepartment implements IObserver {

    @Override
    public void notifyObserver() {
        System.out.println("Accounting department is notified");
    }
}
