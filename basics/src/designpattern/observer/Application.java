package designpattern.observer;

import designpattern.observer.domain.Employee;
import designpattern.observer.observers.AccountingDepartment;
import designpattern.observer.observers.SalesDepartment;
import designpattern.observer.subjects.EmployeeManagementSystem;

public class Application {

    public static void main (String[] args) {
        // Initialize EmployeeManagementSystem and register observers.
        EmployeeManagementSystem managementSystem = new EmployeeManagementSystem();
        managementSystem.registerObserver(new SalesDepartment());
        managementSystem.registerObserver(new AccountingDepartment());

        // Hire employee and notify observers.
        Employee bob = new Employee("Bob");
        managementSystem.hireEmployee(bob);
    }
}
