package designpattern.template;

import designpattern.template.domain.Forecast;

/**
 * This is a child class of the {@link Validator} that defines validation methods for the input {@link Forecast}.
 */
public class ForecastValidator extends Validator<Forecast> {

    @Override
    protected boolean validationOne(Forecast forecast) {
        System.out.println("Validation #1 for Forecast");
        return true;
    }

    @Override
    protected boolean validationTwo(Forecast forecast) {
        System.out.println("Validation #2 for Forecast");
        return true;
    }
}
