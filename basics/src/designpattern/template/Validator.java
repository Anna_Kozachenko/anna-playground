package designpattern.template;

/**
 * This is a small example of the 'Template method pattern' implementation applied to the cash management.
 *
 * Inherit this class if you would like to validate a given object. The method {@link #validate} contains the steps
 * which should be applied to do the validation of an input object. Each validation step is an abstract method which
 * should be re-implemented in the child class.
 *
 * @param <T> the object that needs to be validated
 */
public abstract class Validator<T> {

    protected final boolean validate(T t) {
        if (t == null) {
            return true;
        }

        boolean result = validationOne(t);
        result &= validationTwo(t);

        return result;
    }

    protected abstract boolean validationOne(T t);
    protected abstract boolean validationTwo(T t);
}
