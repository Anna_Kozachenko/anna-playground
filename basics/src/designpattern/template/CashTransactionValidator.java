package designpattern.template;

import designpattern.template.domain.CashTransaction;

/**
 * This is a child class of the {@link Validator} that defines validation methods for the input {@link CashTransaction}.
 */
public class CashTransactionValidator extends Validator<CashTransaction> {

    @Override
    protected boolean validationOne(CashTransaction cashTransaction) {
        System.out.println("Validation #1 for CashTransaction");
        return true;
    }

    @Override
    protected boolean validationTwo(CashTransaction cashTransaction) {
        System.out.println("Validation #2 for CashTransaction");
        return true;
    }
}
